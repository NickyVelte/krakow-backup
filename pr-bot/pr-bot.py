#!/usr/bin/python
# coding: utf-8

import csv
import time
import datetime
import validators
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait

prlist = csv.DictReader(open('pr-list.csv', 'rU')) #name,loginurl,subforumurl,topirnumber,login,pass,advert
krlist = csv.DictReader(open('data.csv', 'rU'))
ad = open('advert', 'r').read()

logfile = open('pr-bot.log', 'a')
logfile.write('['+datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+']: run PR-bot\n')

# textarea id = main-reply
# send button name = submit
# postlink class = permalink (get the last one)

# Disable image, css and flash loading
firefox_profile = webdriver.FirefoxProfile()
firefox_profile.set_preference('permissions.default.stylesheet', 2)
firefox_profile.set_preference('permissions.default.image', 2)
firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
krakow = webdriver.Firefox(firefox_profile)
krakow.get('http://holerajasna.liverolka.ru/login.php')

def krakowlogin(): # login and go to RP topic
    for row in krlist:
        u = row['usr'].decode('utf-8')
        p = row['pass'].decode('utf-8')
    username = krakow.find_element_by_xpath('//input[@size = "25"]')
    password = krakow.find_element_by_xpath('//input[@size = "16"]')
    username.send_keys(u)
    password.send_keys(p, Keys.RETURN)
    time.sleep(3)
    # logfile.write('logged in\n') # TODO optimise logging

def krakowgototopic():
    krakow.get('http://holerajasna.liverolka.ru/viewforum.php?id=15') # pr subforum
    # Get topics (uses last post to go to the last page)
    topics = krakow.find_elements_by_xpath('//td[@class="tcr"]/a')
    url = geturl(topics[1].get_attribute('outerHTML'))
    krakow.get(url)
    textarea = krakow.find_element_by_xpath('//textarea[@id="main-reply"]')
    button = krakow.find_element_by_xpath('//input[@name="submit"]')
    if textarea:
        textarea.send_keys('this is test')
    else:
        newtopic()

def newtopic():
    pass

def geturl(line):  # simple url searcher
    res = line.split('"')
    for r in res:
        if validators.url(r):
            return r.replace('&amp;', '&')
    return ''

def getpost(): # finds last permalink
    links = fox.find_elements_by_xpath('//a[@class="permalink"]')
    link = ''
    for l in links:
        link = geturl(l.get_attribute('outerHTML'))
    return link

krakowlogin()
krakowgototopic()
logfile.close()
# krakow.quit()
